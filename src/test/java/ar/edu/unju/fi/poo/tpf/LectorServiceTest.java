package ar.edu.unju.fi.poo.tpf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.poo.tpf.enums.EstadoLector;
import ar.edu.unju.fi.poo.tpf.model.Lector;
import ar.edu.unju.fi.poo.tpf.model.LectorAsociado;
import ar.edu.unju.fi.poo.tpf.model.LectorNoAsociado;
import ar.edu.unju.fi.poo.tpf.service.LectorService;

@SpringBootTest
public class LectorServiceTest {

	@Autowired
	private LectorService service;
	
	@Test
	@DisplayName("Guardar dos lectores")
	void test1() {
		LectorAsociado asociado1 = new LectorAsociado("Fabian Swift", EstadoLector.PERMITIDO, 100);
		service.guardar(asociado1);
		LectorNoAsociado noAsociado1 = new LectorNoAsociado("Raul Bonadino", EstadoLector.PERMITIDO, "27115006009");
		service.guardar(noAsociado1);
		
		assertEquals(7, service.contar()); // La BD contiene 5 lectores precargados
	}
	
	@Test
	@DisplayName("Buscar lector asociado por Id")
	void test2() {
		LectorAsociado asociado1 = service.buscarIdAsociado(1L);
		
		assertNotNull(asociado1);
	}
	
	@Test
	@DisplayName("Buscar lector no asociado por Id")
	void test3() {
		LectorNoAsociado noAsociado1 = service.buscarIdNoAsociado(3L);
		
		assertNotNull(noAsociado1);
	}
	
	@Test
	@DisplayName("Buscar lectores por nombre")
	void test4() {
		List<Lector> buscados = service.buscarNombre("Fabian");

		assertEquals(2, buscados.size());
	}
	
	@Test
	@DisplayName("Buscar lector por Id")
	void test5() {
		Lector lector = service.buscarId(1L);
		
		assertNotNull(lector);
	}
	
	@Test
	@DisplayName("Actualzar lector")
	void test6() {
		LectorNoAsociado noAsociado = service.buscarIdNoAsociado(4L);
		assertEquals(EstadoLector.VETADO, noAsociado.getEstado());
		
		noAsociado.setEstado(EstadoLector.PERMITIDO);
		service.actualizar(noAsociado);
		
		assertEquals(EstadoLector.PERMITIDO, service.buscarIdNoAsociado(4L).getEstado());
	}
}
