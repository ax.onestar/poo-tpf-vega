package ar.edu.unju.fi.poo.tpf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.poo.tpf.dto.PrestamoDto;
import ar.edu.unju.fi.poo.tpf.enums.EstadoLector;
import ar.edu.unju.fi.poo.tpf.enums.EstadoLibro;
import ar.edu.unju.fi.poo.tpf.exception.ManagerException;
import ar.edu.unju.fi.poo.tpf.model.Libro;
import ar.edu.unju.fi.poo.tpf.service.LectorService;
import ar.edu.unju.fi.poo.tpf.service.LibroService;
import ar.edu.unju.fi.poo.tpf.service.PrestamoService;

@SpringBootTest
public class PrestamoServiceTest {

	@Autowired
	private PrestamoService prestamoService;
	@Autowired
	private LibroService libroService;
	@Autowired
	private LectorService lectorService;
		
	@Test
	@DisplayName("Prestar un libro")
	void test1() {
		assertEquals(EstadoLibro.DISPONIBLE, libroService.buscarId(1L).getEstado());	// Verifica estado libro DISPONIBLE
		PrestamoDto prestamoDto = new PrestamoDto();
		prestamoDto.setIdLector(5L);
		prestamoDto.setIdLibro(1L);
		prestamoService.prestar(prestamoDto);
		assertEquals(2, prestamoService.contar());										// Realiza el préstamo
		assertEquals(EstadoLibro.NO_DISPONIBLE, libroService.buscarId(1L).getEstado());	// Actualizado estado libro a NO_DISPONIBLE
	}
	
	@Test
	@DisplayName("Validar libro disponible")
	void test2() {
		Libro libro = libroService.buscarId(2L);
		libro.setEstado(EstadoLibro.NO_DISPONIBLE);
		libroService.actualizar(libro);
		
		PrestamoDto prestamoDto = new PrestamoDto();
		prestamoDto.setIdLector(1L);
		prestamoDto.setIdLibro(2L);

		assertEquals(EstadoLibro.NO_DISPONIBLE, libroService.buscarId(2L).getEstado());	// Libro id 2, no disponible
		assertEquals(EstadoLector.PERMITIDO, lectorService.buscarId(1L).getEstado());	// Lector id 1, permitido
		assertTrue(prestamoService.contarPrestamos(1L) < 5);							// Lector id 1, no llego MAX prestamos
		assertThrows(ManagerException.class, () -> {
			prestamoService.prestar(prestamoDto);
		});
	}
	
	@Test
	@DisplayName("Validar lector sin multas")
	void test3() {
		PrestamoDto prestamoDto = new PrestamoDto();
		prestamoDto.setIdLector(2L);
		prestamoDto.setIdLibro(3L);
		
		assertEquals(EstadoLibro.DISPONIBLE, libroService.buscarId(3L).getEstado());	// Libro id 3, disponible
		assertEquals(EstadoLector.VETADO, lectorService.buscarId(2L).getEstado());		// Lector id 2, vetado
		assertTrue(prestamoService.contarPrestamos(2L) < 5);							// Lector id 2, no llego MAX prestamos
		assertThrows(ManagerException.class, () -> {
			prestamoService.prestar(prestamoDto);
		});
	}
	
	@Test
	@DisplayName("Validar lector no supera max libros")
	void test4() {
		PrestamoDto prestamoDto = new PrestamoDto();
		prestamoDto.setIdLector(3L);
		prestamoDto.setIdLibro(3L);
		prestamoService.prestar(prestamoDto);
		prestamoDto.setIdLector(3L);
		prestamoDto.setIdLibro(4L);
		prestamoService.prestar(prestamoDto);
		prestamoDto.setIdLector(3L);
		prestamoDto.setIdLibro(5L);
		prestamoService.prestar(prestamoDto);
		
		assertEquals(EstadoLibro.DISPONIBLE, libroService.buscarId(6L).getEstado());	// Libro id 6, disponible
		assertEquals(EstadoLector.PERMITIDO, lectorService.buscarId(3L).getEstado());	// Lector id 3, permitido
		assertEquals(3, lectorService.buscarId(3L).maximoPrestamos());					// Lector id 3, NoAsociado MAX 3 prestamos 
		assertEquals(3, prestamoService.contarPrestamos(3L));							// Lector id 3, llego MAX prestamos
		assertThrows(ManagerException.class, () -> {
			prestamoDto.setIdLector(3L);
			prestamoDto.setIdLibro(6L);
			prestamoService.prestar(prestamoDto);
		});
	}
	
	@Test
	@DisplayName("Devolver un libro")
	void test5() {
		assertEquals(EstadoLibro.NO_DISPONIBLE, libroService.buscarId(1L).getEstado());	// Verifica estado libro NO_DISPONIBLE
		prestamoService.devolver(2L);													// Termina el préstamo
		assertEquals(EstadoLibro.DISPONIBLE, libroService.buscarId(1L).getEstado());	// Actualizado estado libro a DISPONIBLE
	}
	
	@Test
	@DisplayName("Verificar multar lector")
	void test6() {
		assertEquals(EstadoLector.PERMITIDO, lectorService.buscarId(5L).getEstado());	// Verificar estado lector PERMITIDO
		prestamoService.devolver(1L);													// Termina préstamo vencido
		assertEquals(EstadoLector.VETADO, lectorService.buscarId(5L).getEstado());		// Actualizado estado lector a VETADO
	}
}