package ar.edu.unju.fi.poo.tpf;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Suite
@SelectClasses({LibroServiceTest.class, LectorServiceTest.class, PrestamoServiceTest.class})
class ApplicationTests {

}
