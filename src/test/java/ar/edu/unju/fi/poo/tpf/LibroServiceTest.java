package ar.edu.unju.fi.poo.tpf;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.edu.unju.fi.poo.tpf.dto.LibroDto;
import ar.edu.unju.fi.poo.tpf.enums.TipoLibro;
import ar.edu.unju.fi.poo.tpf.exception.ManagerException;
import ar.edu.unju.fi.poo.tpf.model.CodigoIsbn;
import ar.edu.unju.fi.poo.tpf.model.Libro;
import ar.edu.unju.fi.poo.tpf.service.LibroService;

@SpringBootTest
public class LibroServiceTest {

	@Autowired
	private LibroService service;
	
	
	@Test
	@DisplayName("Guardar dos libros con el mismo autor")
	void test1() {
		LibroDto libroDto1 = new LibroDto();
		libroDto1.setTitulo("Yo te dire");
		libroDto1.setAnio(2004);
		libroDto1.setTipo(TipoLibro.POESIA);
		libroDto1.setIdAutor(1L);
		libroDto1.setPrefijo(8);
		libroDto1.setPaisId(8);
		libroDto1.setPrefijoAutor(8);
		libroDto1.setTituloId(8);
		
		LibroDto libroDto2 = new LibroDto();
		libroDto2.setTitulo("Serpiente");
		libroDto2.setAnio(2015);
		libroDto2.setTipo(TipoLibro.POESIA);
		libroDto2.setIdAutor(1L);
		libroDto2.setPrefijo(9);
		libroDto2.setPaisId(9);
		libroDto2.setPrefijoAutor(9);
		libroDto2.setTituloId(9);

		service.guardarLibro(libroDto1);
		service.guardarLibro(libroDto2);
		
		assertEquals(9, service.contar()); // La BD contiene 7 libros precargados
	}
	
	@Test
	@DisplayName("Buscar un libro por Id")
	void test2() {
		Libro libro = service.buscarId(1L);
		assertNotNull(libro);
	}
	
	@Test
	@DisplayName("Buscar libros por Autor")
	void test3() {
		List<Libro> libro = service.buscarTodosAutor("Pablo");
		assertNotNull(libro);
	}
	
	@Test
	@DisplayName("Buscar un libro por Isbn")
	void test4() {
		Libro libro = service.buscarIsbn("1-1-1-1");
		assertNotNull(libro);
	}
	
	@Test
	@DisplayName("Buscar libros por Titulo")
	void test5() {
		List<Libro> libro = service.buscarTodosTitulo("e");
		assertNotNull(libro);
	}
	
	@Test
	@DisplayName("Actualizar un libro")
	void test6() {
		Libro libro = service.buscarId(1L);
		libro.setTitulo("Yo Te Diré vol.2");
		service.actualizar(libro);
		
		assertEquals("Yo Te Diré vol.2", service.buscarId(1L).getTitulo());
	}
	
	@Test
	@DisplayName("Convertir String en CodigoIsbn")
	void test7() {
		String codigo = "1-2-3-4"; // Formato que debe tener al enviarse por HTTP
		CodigoIsbn isbn = CodigoIsbn.ofString(codigo); // Se convierte a un objeto CodigoIsbn
		assertEquals("ISBN: 1-2-3-4", isbn.toString());

		assertThrows(ManagerException.class, () -> {
			Libro libro = service.buscarIsbn("5-5-5-5-"); // Formato incorrecto
		});
		
		assertNotNull(service.buscarIsbn("5-5-5-5")); // Formato correcto
	}
	
	@Test
	@DisplayName("Validar: Isbn no se repite")
	void test8() {
		LibroDto libroDto1 = new LibroDto();
		libroDto1.setTitulo("Lunaticos");
		libroDto1.setAnio(2000);
		libroDto1.setTipo(TipoLibro.NOVELA);
		libroDto1.setIdAutor(3L);
		libroDto1.setPrefijo(9);
		libroDto1.setPaisId(9);
		libroDto1.setPrefijoAutor(9);
		libroDto1.setTituloId(9);
		
		assertThrows(ManagerException.class, () -> {
			service.guardarLibro(libroDto1); // Se intenta guarda libro con Isbn repetido
		});
		
		LibroDto libroDto2 = new LibroDto();
		libroDto2.setTitulo("Terrestres");
		libroDto2.setAnio(2001);
		libroDto2.setTipo(TipoLibro.NOVELA);
		libroDto2.setIdAutor(3L);
		libroDto2.setPrefijo(10);
		libroDto2.setPaisId(10);
		libroDto2.setPrefijoAutor(10);
		libroDto2.setTituloId(10);

		assertDoesNotThrow(() -> {
			service.guardarLibro(libroDto2); // Se guarda libro con Isbn diferente
		});
	}
}
