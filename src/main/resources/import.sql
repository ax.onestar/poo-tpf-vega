INSERT INTO autor VALUES ('2000-12-03', 1, 'Argentino', 'Mariano Moreno');
INSERT INTO autor VALUES ('1990-10-20', 2, 'Mexicana', 'Gloria Murph');
INSERT INTO autor VALUES ('1979-02-02', 3, 'Español', 'Jorge Lamas');
INSERT INTO autor VALUES ('1982-06-13', 4, 'Canadiense', 'Caleb Navor');

INSERT INTO isbn VALUES (1, 1, 1, 1, 1);
INSERT INTO isbn VALUES (2, 2, 2, 2, 2);
INSERT INTO isbn VALUES (3, 3, 3, 3, 3);
INSERT INTO isbn VALUES (4, 4, 4, 4, 4);
INSERT INTO isbn VALUES (5, 5, 5, 5, 5);
INSERT INTO isbn VALUES (6, 6, 6, 6, 6);

INSERT INTO libro VALUES (2023, 1, 1, 1, 'DISPONIBLE', 'NOVELA', 'Luna Llena');
INSERT INTO libro VALUES (2020, 2, 1, 2, 'DISPONIBLE', 'NOVELA', 'Moraleja del Gato Audaz');
INSERT INTO libro VALUES (1999, 3, 2, 3, 'DISPONIBLE', 'ENSAYO', 'Los Meteoritos');
INSERT INTO libro VALUES (2001, 4, 3, 4, 'DISPONIBLE', 'POESIA', 'Corazones Rotos');
INSERT INTO libro VALUES (2006, 5, 3, 5, 'DISPONIBLE', 'TEATRO', 'Troya');
INSERT INTO libro VALUES (1998, 6, 4, 6, 'DISPONIBLE', 'ENSAYO', 'Cyber Seguridad - Nueva Era');

INSERT INTO lector VALUES (100, 1, 'LA', 'PERMITIDO', 'Arturo Caranuez', null);
INSERT INTO lector VALUES (101, 2, 'LA', 'VETADO', 'Mariana Luque', null);

INSERT INTO lector VALUES (null, 3, 'LN', 'PERMITIDO', 'Fabian Roque', '22801005003');
INSERT INTO lector VALUES (null, 4, 'LN', 'VETADO', 'Hernesto Laprida', '20506005009');