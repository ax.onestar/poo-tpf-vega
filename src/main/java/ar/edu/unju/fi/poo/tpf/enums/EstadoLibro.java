package ar.edu.unju.fi.poo.tpf.enums;

public enum EstadoLibro {
	DISPONIBLE, NO_DISPONIBLE;
}
