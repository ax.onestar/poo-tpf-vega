package ar.edu.unju.fi.poo.tpf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unju.fi.poo.tpf.dto.LibroDto;
import ar.edu.unju.fi.poo.tpf.exception.ManagerException;
import ar.edu.unju.fi.poo.tpf.model.Autor;
import ar.edu.unju.fi.poo.tpf.model.Libro;
import ar.edu.unju.fi.poo.tpf.service.LibroService;

@RestController
@RequestMapping("/api/libros")
public class LibroController {
	
	@Autowired
	LibroService service;
	
	
	@PostMapping("/agregarLibro")
	public ResponseEntity<?> agregarLibro(@RequestBody LibroDto libroDto) {
		String mensaje;
		try {
			service.guardarLibro(libroDto);
			mensaje = "Libro guardado correctamente.";
			return ResponseEntity.ok(mensaje);
		} catch (ManagerException e) {
			mensaje = e.getMessage();
			return ResponseEntity.internalServerError().body(mensaje);
		}
	}
	
	@PostMapping("/agregarAutor")
	public ResponseEntity<?> agregarAutor(@RequestBody Autor autor) {
		String mensaje = "Autor guardado correctamente.";
		service.guardarAutor(autor);
		return ResponseEntity.ok(mensaje);
	}
	
	@GetMapping("/buscarTitulo/{titulo}")
	public ResponseEntity<List<Libro>> buscarPorTitulo(@PathVariable("titulo") String titulo) {
		List<Libro> librosBuscados = service.buscarTodosTitulo(titulo);
		return ResponseEntity.ok(librosBuscados);
	}
	
	@GetMapping("/buscarAutor/{autor}")
	public ResponseEntity<List<Libro>> buscarPorAutor(@PathVariable("autor") String nombreAutor) {
		List<Libro> librosBuscados = service.buscarTodosAutor(nombreAutor);
		return ResponseEntity.ok(librosBuscados);
	}
	
	@GetMapping("/buscarIsbn/{isbn}")
	public ResponseEntity<Libro> buscarPorIsbn(@PathVariable("isbn") String isbn) {
		Libro libroBuscado = service.buscarIsbn(isbn);
		return ResponseEntity.ok(libroBuscado);
	}
}
