package ar.edu.unju.fi.poo.tpf.enums;

public enum EstadoLector {
	VETADO, PERMITIDO;
}
