package ar.edu.unju.fi.poo.tpf.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity(name = "autor")
public class Autor extends Persona {
	
	@Column(name = "nacionalidad")
	private String nacionalidad;
	
	@Column(name = "fch_nacimiento")
	private LocalDate fechaNacimiento;
	
	
	public Autor() {}
	
	public Autor(String nombre, String nacionalidad, LocalDate fechaNacimiento) {
		super(nombre);
		this.nacionalidad = nacionalidad;
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}
	
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public String toString() {
		return "Autor [id=" + getId() + ", nombre=" + getNombre() + ", nacionalidad=" + nacionalidad + ", fechaNacimiento="
		+ fechaNacimiento + "]";
	}

	
}
