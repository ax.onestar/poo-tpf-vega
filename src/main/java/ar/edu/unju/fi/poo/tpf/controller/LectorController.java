package ar.edu.unju.fi.poo.tpf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unju.fi.poo.tpf.model.Lector;
import ar.edu.unju.fi.poo.tpf.model.LectorAsociado;
import ar.edu.unju.fi.poo.tpf.model.LectorNoAsociado;
import ar.edu.unju.fi.poo.tpf.service.LectorService;

@RestController
@RequestMapping("/api/lectores")
public class LectorController {
	
	@Autowired
	LectorService service;
	
	
	@PostMapping("/agregarAsociado")
	public ResponseEntity<?> agregarAsociado(@RequestBody LectorAsociado lector) {
		service.guardar(lector);
		String mensaje = "Lector agregado correctamente.";
		return ResponseEntity.ok(mensaje);
	}
	
	@PostMapping("/agregarNoAsociado")
	public ResponseEntity<?> agregarNoAsociado(@RequestBody LectorNoAsociado lector) {
		service.guardar(lector);
		String mensaje = "Lector agregado correctamente.";
		return ResponseEntity.ok(mensaje);
	}
	
	@GetMapping("/buscarId/{id}")
	public ResponseEntity<Lector> buscarId(@PathVariable("id") Long id) {
		Lector lectorBuscado = service.buscarId(id);
		return ResponseEntity.ok(lectorBuscado);
	}
	
	@GetMapping("/buscarNombre/{nombre}")
	public ResponseEntity<List<Lector>> buscarNombre(@PathVariable("nombre") String nombre) {
		List<Lector> lectoresBuscados = service.buscarNombre(nombre);
		return ResponseEntity.ok(lectoresBuscados);
	}
}