package ar.edu.unju.fi.poo.tpf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.poo.tpf.model.Prestamo;
import ar.edu.unju.fi.poo.tpf.model.Lector;
import ar.edu.unju.fi.poo.tpf.enums.EstadoPrestamo;
import java.util.List;
import ar.edu.unju.fi.poo.tpf.model.Libro;




@Repository
public interface PrestamoRepository extends CrudRepository<Prestamo, Long>{
	
	public List<Prestamo> findByLectorAndLibro(Lector lector, Libro libro);
	
	public long countByLectorAndEstado(Lector lector, EstadoPrestamo estado);
}
