package ar.edu.unju.fi.poo.tpf.model;

import ar.edu.unju.fi.poo.tpf.enums.Constante;
import ar.edu.unju.fi.poo.tpf.enums.EstadoLector;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value = "LA")
public class LectorAsociado extends Lector {

	@Column(name = "nro_asociado")
	private int numeroAsociado;
	

	public LectorAsociado() {}
	
	public LectorAsociado(String nombre, EstadoLector estado, int numeroAsociado) {
		super(nombre, estado);
		this.numeroAsociado = numeroAsociado;
	}

	public int getNumeroAsociado() {
		return numeroAsociado;
	}
	
	public void setNumeroAsociado(int numeroAsociado) {
		this.numeroAsociado = numeroAsociado;
	}

	@Override
	public int maximoPrestamos() {
		return Constante.LA_MAX_PRESTAMOS.getValor();
	}

	@Override
	public int maximoDiasPrestamo() {
		return Constante.LA_MAX_DIAS_PRESTAMO.getValor();
	}

	@Override
	public String toString() {
		return "LectorAsociado [id=" + getId() + ", nombre=" + getNombre() + ", numeroAsociado=" + numeroAsociado + ", estado=" + getEstado() + "]";
	}

}
