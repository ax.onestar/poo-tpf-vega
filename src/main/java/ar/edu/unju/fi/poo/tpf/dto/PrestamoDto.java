package ar.edu.unju.fi.poo.tpf.dto;

import java.io.Serializable;

public class PrestamoDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	// id, fechaPrestamo, fechaDevolucion, estado, son valores que  son controlados por Service, Repository y Model
	private Long idLector;
	private Long idLibro;
	
	public Long getIdLector() {
		return idLector;
	}
	public void setIdLector(Long idLector) {
		this.idLector = idLector;
	}
	public Long getIdLibro() {
		return idLibro;
	}
	public void setIdLibro(Long idLibro) {
		this.idLibro = idLibro;
	}
}
