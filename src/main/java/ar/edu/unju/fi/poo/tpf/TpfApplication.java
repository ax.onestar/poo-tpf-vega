package ar.edu.unju.fi.poo.tpf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpfApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpfApplication.class, args);
	}

}
