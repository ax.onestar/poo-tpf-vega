package ar.edu.unju.fi.poo.tpf.model;

import ar.edu.unju.fi.poo.tpf.enums.EstadoLector;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;

@Entity(name = "lector")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "clase")
public abstract class Lector extends Persona {
	
	@Enumerated(EnumType.STRING)
	private EstadoLector estado;
	
	
	public Lector() {}

	public Lector(String nombre, EstadoLector estado) {
		super(nombre);
		this.estado = estado;
	}

	public EstadoLector getEstado() {
		return estado;
	}
	
	public void setEstado(EstadoLector estado) {
		this.estado = estado;
	}
	
	public abstract int maximoPrestamos();
	public abstract int maximoDiasPrestamo();

}
