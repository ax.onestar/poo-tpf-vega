package ar.edu.unju.fi.poo.tpf.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.poo.tpf.model.Autor;
import ar.edu.unju.fi.poo.tpf.model.CodigoIsbn;
import ar.edu.unju.fi.poo.tpf.model.Libro;


@Repository
public interface LibroRepository extends CrudRepository<Libro, Long>{
	
	public Libro findByIsbn(CodigoIsbn isbn);
	
	public List<Libro> findByAutor(Autor autor);
	
	public List<Libro> findByTituloContainingIgnoreCase(String titulo);
}
