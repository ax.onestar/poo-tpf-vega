package ar.edu.unju.fi.poo.tpf.enums;

public enum TipoLibro {
	NOVELA, TEATRO, POESIA, ENSAYO;
}
