package ar.edu.unju.fi.poo.tpf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.unju.fi.poo.tpf.dto.PrestamoDto;
import ar.edu.unju.fi.poo.tpf.exception.ManagerException;
import ar.edu.unju.fi.poo.tpf.model.Prestamo;
import ar.edu.unju.fi.poo.tpf.service.PrestamoService;

@RestController
@RequestMapping("/api/prestamos")
public class PrestamoController {

	@Autowired
	PrestamoService service;
	
	
	@PostMapping("/prestar")
	public ResponseEntity<?> registraPrestamo(@RequestBody PrestamoDto prestamoDto) {
		String mensaje;
		try {
			service.prestar(prestamoDto);
			mensaje = "Prestamo registrado correctamente.";
			return ResponseEntity.ok(mensaje);
		} catch (ManagerException e) {
			mensaje = e.getMessage();
			return ResponseEntity.internalServerError().body(mensaje);
		}
	}
	
	@PutMapping("/devolver/{id}")
	public ResponseEntity<?> registrarDevolucion(@PathVariable("id") Long id) {
		service.devolver(id);
		String mensaje = "Devolucion resgistrada correctamente.";
		return ResponseEntity.ok(mensaje);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Prestamo> obtener(@PathVariable("id") Long id) {
		Prestamo prestamo = service.buscarId(id);
		return ResponseEntity.ok(prestamo);
	}
}
