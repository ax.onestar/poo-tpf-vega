package ar.edu.unju.fi.poo.tpf.model;

import ar.edu.unju.fi.poo.tpf.enums.Constante;
import ar.edu.unju.fi.poo.tpf.enums.EstadoLector;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value = "LN")
public class LectorNoAsociado extends Lector {
	
	@Column(name = "nro_cuil")
	private String numeroCuil;
	

	public LectorNoAsociado() {}
	
	public LectorNoAsociado(String nombre, EstadoLector estado, String numeroCuil) {
		super(nombre, estado);
		this.numeroCuil = numeroCuil;
	}

	public String getNumeroCuil() {
		return numeroCuil;
	}
	
	public void setNumeroCuil(String numeroCuil) {
		this.numeroCuil = numeroCuil;
	}

	@Override
	public int maximoPrestamos() {
		return Constante.LN_MAX_PRESTAMOS.getValor();
	}

	@Override
	public int maximoDiasPrestamo() {
		return Constante.LN_MAX_DIAS_PRESTAMO.getValor();
	}
	
	@Override
	public String toString() {
		return "LectorAsociado [id=" + getId() + ", nombre=" + getNombre() + ", numeroCuil=" + numeroCuil + ", estado=" + getEstado() + "]";
	}

}
