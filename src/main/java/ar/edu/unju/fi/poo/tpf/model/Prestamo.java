package ar.edu.unju.fi.poo.tpf.model;

import java.time.LocalDate;

import ar.edu.unju.fi.poo.tpf.enums.EstadoPrestamo;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity(name = "prestamo")
public class Prestamo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "fch_prestamo")
	private LocalDate fechaPrestamo;
	
	@Column(name = "fch_devolucion")
	private LocalDate fechaDevolucion;
	
	@ManyToOne
	@JoinColumn(name = "id_lector")
	private Lector lector;
	
	@ManyToOne
	@JoinColumn(name = "id_libro")
	private Libro libro;
	
	@Enumerated(EnumType.STRING)
	private EstadoPrestamo estado;
	
	
	public Prestamo() {}

	public Prestamo(LocalDate fechaPrestamo, LocalDate fechaDevolucion, Lector lector, Libro libro,
			EstadoPrestamo estado) {
		super();
		this.fechaPrestamo = fechaPrestamo;
		this.fechaDevolucion = fechaDevolucion;
		this.lector = lector;
		this.libro = libro;
		this.estado = estado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(LocalDate fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public LocalDate getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(LocalDate fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public Lector getLector() {
		return lector;
	}

	public void setLector(Lector lector) {
		this.lector = lector;
	}

	public Libro getLibro() {
		return libro;
	}

	public void setLibro(Libro libro) {
		this.libro = libro;
	}

	public EstadoPrestamo getEstado() {
		return estado;
	}

	public void setEstado(EstadoPrestamo estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Prestamo [id=" + id + ", fechaPrestamo=" + fechaPrestamo + ", fechaDevolucion=" + fechaDevolucion
				+ ", lector=" + lector + ", libro=" + libro + ", estado=" + estado + "]";
	}
}
