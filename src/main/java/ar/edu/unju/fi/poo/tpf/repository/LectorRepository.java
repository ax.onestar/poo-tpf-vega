package ar.edu.unju.fi.poo.tpf.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.poo.tpf.model.Lector;

@Repository
public interface LectorRepository extends CrudRepository<Lector, Long>{
	
	public List<Lector> findByNombreContainingIgnoreCase(String nombre);

}
