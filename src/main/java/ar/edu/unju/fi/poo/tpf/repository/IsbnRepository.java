package ar.edu.unju.fi.poo.tpf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.poo.tpf.model.CodigoIsbn;

@Repository
public interface IsbnRepository extends CrudRepository<CodigoIsbn, Long>{
	
	public CodigoIsbn findByPrefijoAndIdPaisAndPrefijoAutorAndIdTitulo(int prefijo, int idPais, int prefijoAutor, int idTitulo);

}
