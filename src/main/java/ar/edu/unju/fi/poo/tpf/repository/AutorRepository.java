package ar.edu.unju.fi.poo.tpf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.edu.unju.fi.poo.tpf.model.Autor;

@Repository
public interface AutorRepository extends CrudRepository<Autor, Long>{
	
	public Autor findByNombreContainingIgnoreCase(String nombre);
	
	public Autor findByNombre(String nombre);
	
}
