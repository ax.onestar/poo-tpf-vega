package ar.edu.unju.fi.poo.tpf.enums;

public enum EstadoPrestamo {
	PRESTADO, DEVUELTO;
}
