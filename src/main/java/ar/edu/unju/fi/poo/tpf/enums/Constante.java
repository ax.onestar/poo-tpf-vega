package ar.edu.unju.fi.poo.tpf.enums;

public enum Constante {
	// LectorAsociado
	LA_MAX_DIAS_PRESTAMO(7),	
	LA_MAX_PRESTAMOS(5),
	
	// LectorNoAsociado
	LN_MAX_DIAS_PRESTAMO(2),
	LN_MAX_PRESTAMOS(3);

	private int valor;
	Constante(int valor) {
		this.valor = valor;
	}
	
	public int getValor() {
		return valor;
	}
}
