package ar.edu.unju.fi.poo.tpf.dto;

import java.io.Serializable;

import ar.edu.unju.fi.poo.tpf.enums.TipoLibro;

public class LibroDto implements Serializable{

	private static final long serialVersionUID = 1L;

	// Para el libro:
	private String titulo;
	private int anio;
	private TipoLibro tipo;
	private Long idAutor;
	
	// Para su ISBN:
	private int prefijo;
	private int paisId;
	private int prefijoAutor;
	private int tituloId;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public TipoLibro getTipo() {
		return tipo;
	}
	public void setTipo(TipoLibro tipo) {
		this.tipo = tipo;
	}
	public Long getIdAutor() {
		return idAutor;
	}
	public void setIdAutor(Long idAutor) {
		this.idAutor = idAutor;
	}
	public int getPrefijo() {
		return prefijo;
	}
	public void setPrefijo(int prefijo) {
		this.prefijo = prefijo;
	}
	public int getPaisId() {
		return paisId;
	}
	public void setPaisId(int paisId) {
		this.paisId = paisId;
	}
	public int getPrefijoAutor() {
		return prefijoAutor;
	}
	public void setPrefijoAutor(int prefijoAutor) {
		this.prefijoAutor = prefijoAutor;
	}
	public int getTituloId() {
		return tituloId;
	}
	public void setTituloId(int tituloId) {
		this.tituloId = tituloId;
	}
	
}
