package ar.edu.unju.fi.poo.tpf.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.poo.tpf.dto.PrestamoDto;
import ar.edu.unju.fi.poo.tpf.enums.EstadoLector;
import ar.edu.unju.fi.poo.tpf.enums.EstadoLibro;
import ar.edu.unju.fi.poo.tpf.enums.EstadoPrestamo;
import ar.edu.unju.fi.poo.tpf.exception.ManagerException;
import ar.edu.unju.fi.poo.tpf.model.Lector;
import ar.edu.unju.fi.poo.tpf.model.Libro;
import ar.edu.unju.fi.poo.tpf.model.Prestamo;
import ar.edu.unju.fi.poo.tpf.repository.LectorRepository;
import ar.edu.unju.fi.poo.tpf.repository.LibroRepository;
import ar.edu.unju.fi.poo.tpf.repository.PrestamoRepository;

@Service
public class PrestamoService {

	@Autowired
	private PrestamoRepository prestamoRepo;
	@Autowired
	private LibroRepository libroRepo;
	@Autowired
	private LectorRepository lectorRepo;
	
	/**
	 * Realizar un nuevo préstamo de un Libro a un Lector.
	 * @param prestamoDto DTO con los ID de Libro y Lector
	 * @throws ManagerException Si no se cumplen las restricciones se emitirá una excepción
	 */
	public void prestar(PrestamoDto prestamoDto) throws ManagerException {
		Lector lector = lectorRepo.findById(prestamoDto.getIdLector()).get();
		Libro libro = libroRepo.findById(prestamoDto.getIdLibro()).get();
		libroNoDisponible(libro);
		lectorConMulta(lector);
		lectorLLegoLimite(lector);
		
		Prestamo nuevoPrestamo = definirDatos(lector, libro);
		prestamoRepo.save(nuevoPrestamo);
		actualizarEstadoLibro(libro, EstadoLibro.NO_DISPONIBLE);
	}
	private void libroNoDisponible(Libro libro) throws ManagerException {
		if (libro.getEstado()==EstadoLibro.NO_DISPONIBLE) {
			throw new ManagerException("Libro " + libro.getEstado().name() + ". No puede prestar un libro No Disponible.");
		}
	}
	private void lectorConMulta(Lector lector) throws ManagerException {
		if (lector.getEstado()==EstadoLector.VETADO) {
			throw new ManagerException("Lector " + lector.getEstado().name() + ". No puede prestar a un lector con multas.");
		}
	}
	private void lectorLLegoLimite(Lector lector) throws ManagerException {
		long cantPrestamosPendientes = prestamoRepo.countByLectorAndEstado(lector, EstadoPrestamo.PRESTADO); 
		if (cantPrestamosPendientes==lector.maximoPrestamos()) {
			throw new ManagerException("Lector con " + cantPrestamosPendientes + " prestamos sin devolver. Ha llegado a su limite maximo.");
		}
	}
	private Prestamo definirDatos(Lector lector, Libro libro) {
		Prestamo prestamo = new Prestamo();
		prestamo.setLector(lector);
		prestamo.setLibro(libro);
		prestamo.setFechaPrestamo(LocalDate.now());
		prestamo.setFechaDevolucion(LocalDate.now().plusDays(lector.maximoDiasPrestamo()));
		prestamo.setEstado(EstadoPrestamo.PRESTADO);
		return prestamo;
	}
	private void actualizarEstadoLibro(Libro libro, EstadoLibro nuevoEstado) {
		libro.setEstado(nuevoEstado);
		libroRepo.save(libro);
	}
	
	/**
	 * Terminar un préstamo previamente registrado. También actualiza los estados de Libro y Lector.
	 * @param idPrestamo ID del préstamo
	 */
	public void devolver(Long idPrestamo) {
		Prestamo prestamoAct = prestamoRepo.findById(idPrestamo).get();
		prestamoAct.setEstado(EstadoPrestamo.DEVUELTO);
		prestamoRepo.save(prestamoAct);
		multarLector(prestamoAct);
		actualizarEstadoLibro(prestamoAct.getLibro(), EstadoLibro.DISPONIBLE);
	}
	private void multarLector(Prestamo prestamo) {
		if (ChronoUnit.DAYS.between(prestamo.getFechaDevolucion(), LocalDate.now())>0) {
			prestamo.getLector().setEstado(EstadoLector.VETADO);
			lectorRepo.save(prestamo.getLector());
		}
	}
	
	/**
	 * Obtener un lista completa con todos los prestamos registrados.
	 * @return Lista de prestamos
	 */
	public List<Prestamo> listar() {
		List<Prestamo> listado = new ArrayList<>();
		prestamoRepo.findAll().forEach(listado::add);
		return listado;
	}
	
	
	/**
	 * Buscar un préstamo por ID.
	 * @param id ID del préstamo
	 * @return Préstamo buscado
	 */
	public Prestamo buscarId(Long id) {
		return prestamoRepo.findById(id).get();
	}
	
	
	/**
	 * Contar la cantidad total de prestamos registrados.
	 * @return Cantidad de prestamos registrados
	 */
	public long contar() {
		return prestamoRepo.count();
	}
	
	/**
	 * Contar la cantidad de prestamos sin devolver, de un Lector específico.
	 * @param idLector ID del Lector
	 * @return Cantidad de prestamos sin devolver, del Lector
	 */
	public long contarPrestamos(Long idLector) {
		Lector lector = lectorRepo.findById(idLector).get();
		return prestamoRepo.countByLectorAndEstado(lector, EstadoPrestamo.PRESTADO);
	}
}
