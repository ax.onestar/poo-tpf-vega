package ar.edu.unju.fi.poo.tpf.model;

import ar.edu.unju.fi.poo.tpf.enums.EstadoLibro;
import ar.edu.unju.fi.poo.tpf.enums.TipoLibro;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity(name = "libro")
public class Libro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "titulo")
	private String titulo;
	
	@Column(name = "anio")
	private int anio;
	
	@Enumerated(EnumType.STRING)
	private TipoLibro tipo;
	
	@ManyToOne
	@JoinColumn(name = "id_autor")
	private Autor autor;
	
	@Enumerated(EnumType.STRING)
	private EstadoLibro estado;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_isbn")
	private CodigoIsbn isbn;
	
	
	public Libro() {}

	public Libro(String titulo, int anio, TipoLibro tipo, Autor autor, EstadoLibro estado, int prefijo, int idPais, int prefijoAutor, int idTitulo) {
		super();
		this.titulo = titulo;
		this.anio = anio;
		this.tipo = tipo;
		this.autor = autor;
		this.estado = estado;
		this.isbn = new CodigoIsbn(prefijo, idPais, prefijoAutor, idTitulo);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}
	
	public TipoLibro getTipo() {
		return tipo;
	}

	public void setTipo(TipoLibro tipo) {
		this.tipo = tipo;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}
	
	public EstadoLibro getEstado() {
		return estado;
	}
	
	public void setEstado(EstadoLibro estado) {
		this.estado = estado;
	}

	public CodigoIsbn getIsbn() {
		return isbn;
	}

	public void setIsbn(CodigoIsbn isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		return "Libro [id=" + id + ", titulo=" + titulo + ", anio=" + anio + ", tipo=" + tipo + ", autor=" + autor
				+ ", isbn=" + isbn + "]";
	}
}
