package ar.edu.unju.fi.poo.tpf.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.poo.tpf.dto.LibroDto;
import ar.edu.unju.fi.poo.tpf.enums.EstadoLibro;
import ar.edu.unju.fi.poo.tpf.exception.ManagerException;
import ar.edu.unju.fi.poo.tpf.model.Autor;
import ar.edu.unju.fi.poo.tpf.model.CodigoIsbn;
import ar.edu.unju.fi.poo.tpf.model.Libro;
import ar.edu.unju.fi.poo.tpf.repository.AutorRepository;
import ar.edu.unju.fi.poo.tpf.repository.IsbnRepository;
import ar.edu.unju.fi.poo.tpf.repository.LibroRepository;

@Service
public class LibroService {

	@Autowired
	private LibroRepository libroRepo;
	@Autowired
	private AutorRepository autorRepo;
	@Autowired
	private IsbnRepository isbnRepo;

	/**
	 * Guarda un nuevo libro.
	 * @param libroDto DTO con los datos del Libro y su CodigoIsbn
	 * @throws ManagerException Si no se cumplen las restricciones se emitirá una
	 *                          excepción
	 */
	public void guardarLibro(LibroDto libroDto) throws ManagerException {
		Autor autor = autorRepo.findById(libroDto.getIdAutor()).get();
		Libro nuevoLibro = new Libro(libroDto.getTitulo(), libroDto.getAnio(), libroDto.getTipo(), autor,
				EstadoLibro.DISPONIBLE, libroDto.getPrefijo(), libroDto.getPaisId(), libroDto.getPrefijoAutor(),
				libroDto.getTituloId());
		isbnRepetido(nuevoLibro.getIsbn());
		libroRepo.save(nuevoLibro);
	}

	private void isbnRepetido(CodigoIsbn c) throws ManagerException {
		CodigoIsbn isbnRepetido = isbnRepo.findByPrefijoAndIdPaisAndPrefijoAutorAndIdTitulo(c.getPrefijo(),
				c.getIdPais(), c.getPrefijoAutor(), c.getIdTitulo());
		if (isbnRepetido != null) {
			throw new ManagerException(
					isbnRepetido + " repetido. No se puden alamcenar libros con codigo ISBN repetidos.");
		}
	}

	public void guardarAutor(Autor autor) {
		autorRepo.save(autor);
	}

	/**
	 * Actualiza el Libro enviado.
	 * @param libro Libro con ID previamente registrado.
	 */
	public void actualizar(Libro libro) {
		libroRepo.save(libro);
	}

	/**
	 * Obtener un lista completa con todos los Libros registrados
	 * @return Lista de libros
	 */
	public List<Libro> listar() {
		List<Libro> listado = new ArrayList<>();
		libroRepo.findAll().forEach(listado::add);
		return listado;
	}

	/**
	 * Busca un Libro mediante su ID especificado.
	 * @param id ID del libro a buscar
	 * @return Libro buscado
	 */
	public Libro buscarId(Long id) {
		return libroRepo.findById(id).get();
	}

	/**
	 * Busca un Libro mediante su Isbn. El código Isbn pasa por parámetro debe tener
	 * el formato '0-0-0-0', dende 0 representa cualquier entero.
	 * @param isbn String con formato 0-0-0-0
	 * @return Libro buscado
	 * @throws ManagerException Cuando el String enviado no tiene el formato correcto, se lanzará un excepción
	 */
	public Libro buscarIsbn(String isbn) throws ManagerException {
		CodigoIsbn c = CodigoIsbn.ofString(isbn);
		CodigoIsbn isbnBuscado = isbnRepo.findByPrefijoAndIdPaisAndPrefijoAutorAndIdTitulo(c.getPrefijo(),
				c.getIdPais(), c.getPrefijoAutor(), c.getIdTitulo());
		return libroRepo.findByIsbn(isbnBuscado);
	}

	/**
	 * Buscar todos los Libros que contengan la cadena enviada por parámetro en su titulo.
	 * @param titulo Cadena con todo o parte del titulo
	 * @return Lista de libros buscados
	 */
	public List<Libro> buscarTodosTitulo(String titulo) {
		return libroRepo.findByTituloContainingIgnoreCase(titulo);
	}

	/**
	 * Busca todos los Libros que tengan determinado Autor.
	 * @param nombre Nombre completo del Autor
	 * @return Lista de libros buscados
	 */
	public List<Libro> buscarTodosAutor(String nombre) {
		Autor autor = autorRepo.findByNombre(nombre);
		return libroRepo.findByAutor(autor);
	}

	/**
	 * Contar la cantidad total de Libros registrados.
	 * @return Cantidad de libros
	 */
	public long contar() {
		return libroRepo.count();
	}
}
