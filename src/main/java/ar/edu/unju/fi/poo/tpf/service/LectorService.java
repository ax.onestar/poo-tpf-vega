package ar.edu.unju.fi.poo.tpf.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.poo.tpf.model.Lector;
import ar.edu.unju.fi.poo.tpf.model.LectorAsociado;
import ar.edu.unju.fi.poo.tpf.model.LectorNoAsociado;
import ar.edu.unju.fi.poo.tpf.repository.LectorRepository;

@Service
public class LectorService {

	@Autowired
	private LectorRepository lectorRepo;

	/**
	 * Guardar un nuevo libro.
	 * @param lector Lector con todos sus datos.
	 */
	public void guardar(Lector lector) {
		lectorRepo.save(lector);
	}

	/**
	 * Actualizar el Lector enviado.
	 * @param lector Lector con ID previamente registrado
	 */
	public void actualizar(Lector lector) {
		lectorRepo.save(lector);
	}

	/**
	 * Obtener una lista completa con todos los Lectores registrados.
	 * @return Lista de lectores
	 */
	public List<Lector> listar() {
		List<Lector> listado = new ArrayList<>();
		lectorRepo.findAll().forEach(listado::add);
		return listado;
	}

	/**
	 * Obtener una lista con todos los Lectores que en su nombre contengan una
	 * cadena especificada.
	 * @param nombre Cadena con el nombre completo o parcial
	 * @return Lista de lectores
	 */
	public List<Lector> buscarNombre(String nombre) {
		return lectorRepo.findByNombreContainingIgnoreCase(nombre);
	}

	/**
	 * Buscar un Lector de cualquier subtipo (asociado o no asociado), mediante su
	 * ID especificado.
	 * @param id ID del Lector
	 * @return Lector buscado
	 */
	public Lector buscarId(Long id) {
		return lectorRepo.findById(id).get();
	}

	/**
	 * Buscar un Lector del subtipo Asociado por su ID especificado.
	 * @param id ID del Lector
	 * @return LectorAsociado buscado
	 */
	public LectorAsociado buscarIdAsociado(Long id) {
		Lector lector = lectorRepo.findById(id).get();
		if (lector instanceof LectorAsociado) {
			return (LectorAsociado) lector;
		}
		return null;

	}

	/**
	 * Buscar un Lector del subtipo NoAsociado por su ID especificado
	 * @param id ID del Lector
	 * @return LectorNoAsociado buscado
	 */
	public LectorNoAsociado buscarIdNoAsociado(Long id) {
		Lector lector = lectorRepo.findById(id).get();
		if (lector instanceof LectorNoAsociado) {
			return (LectorNoAsociado) lector;
		}
		return null;
	}

	/**
	 * Contar la cantidad total de Lectores registrados.
	 * @return Cantidad de lectores
	 */
	public long contar() {
		return lectorRepo.count();
	}
}
