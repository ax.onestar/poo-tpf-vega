package ar.edu.unju.fi.poo.tpf.model;

import java.util.Objects;

import ar.edu.unju.fi.poo.tpf.exception.ManagerException;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity(name = "isbn")
public class CodigoIsbn {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "prefijo")
	private int prefijo;
	
	@Column(name = "pais_id")
	private int idPais;
	
	@Column(name = "prefijo_autor")
	private int prefijoAutor;
	
	@Column(name = "titulo_id")
	private int idTitulo;
	
	
	public CodigoIsbn() {}
	
	public CodigoIsbn(int prefijo, int idPais, int prefijoAutor, int idTitulo) {
		super();
		this.prefijo = prefijo;
		this.idPais = idPais;
		this.prefijoAutor = prefijoAutor;
		this.idTitulo = idTitulo;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(int prefijo) {
		this.prefijo = prefijo;
	}

	public int getIdPais() {
		return idPais;
	}

	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}

	public int getPrefijoAutor() {
		return prefijoAutor;
	}

	public void setPrefijoAutor(int prefijoAuto) {
		this.prefijoAutor = prefijoAuto;
	}

	public int getIdTitulo() {
		return idTitulo;
	}

	public void setIdTitulo(int idLibro) {
		this.idTitulo = idLibro;
	}

	@Override
	public String toString() {
		return "ISBN: " + prefijo + "-" + idPais + "-" + prefijoAutor + "-" + idTitulo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idPais, idTitulo, prefijo, prefijoAutor);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodigoIsbn other = (CodigoIsbn) obj;
		return idPais == other.idPais && idTitulo == other.idTitulo && prefijo == other.prefijo
				&& prefijoAutor == other.prefijoAutor;
	}
	
	public static CodigoIsbn ofString(String codigoString) throws ManagerException {
		String secuencia = new String();
		CodigoIsbn convertido = new CodigoIsbn();
		try {
			int j = 0;
			for (int i=0; i < codigoString.length(); i++) {
				if (codigoString.charAt(i)!='-') {
					secuencia += codigoString.charAt(i);
				} else {
					switch (j) {
					case 0:
						convertido.setPrefijo(Integer.valueOf(secuencia));
						break;
					case 1:
						convertido.setIdPais(Integer.valueOf(secuencia));
						break;
					case 2:
						convertido.setPrefijoAutor(Integer.valueOf(secuencia));
						break;
					}
					j++;
					secuencia = new String();
				}
			}
			convertido.setIdTitulo(Integer.valueOf(secuencia));
		} catch (Exception e) {
			throw new ManagerException("String '" + codigoString + "' no tiene el formato 0-0-0-0. No se puede convertir a un CodigoIsbn.");
		}
		return convertido;
	}

}
